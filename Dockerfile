FROM jenkins/ssh-agent:jdk17

RUN \
# Update
apt-get update -y && \
# Install Unzip
apt-get install unzip -y && \
# need wget
apt-get install wget -y && \
# nano
apt-get install nano -y && \
# curl
apt-get install curl -y

################################
# Install Terraform
################################

# Download terraform for linux
RUN wget https://releases.hashicorp.com/terraform/1.8.2/terraform_1.8.2_linux_amd64.zip

# Unzip
RUN unzip terraform_1.8.2_linux_amd64.zip

# Move to local bin
RUN mv terraform /usr/local/bin/
# Check that it's installed
RUN terraform --version 

###############################
# Install AWS CLI
###############################
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

RUN unzip awscliv2.zip

RUN ./aws/install
# # add aws cli location to path
# ENV PATH=~/.local/bin:$PATH

# # Adds local templates directory and contents in /usr/local/terrafrom-templates
# ADD templates /usr/local/bin/templates

# RUN mkdir ~/.aws && touch ~/.aws/credentials


###############################
# Install Kubectl
###############################
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl

RUN chmod +x ./kubectl

RUN mv ./kubectl /usr/local/bin


###############################
# Install Helm
###############################
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3

RUN chmod 700 get_helm.sh

RUN ./get_helm.sh

