# Configure the AWS provider
provider "aws" {
  region = var.region
}

# VPC Module
module "vpc" {
  source = "./vpc"

  # Pass the required variables to the VPC module
  vpc_name       = var.vpc_name
  cluster_name   = var.cluster_name
}

# EKS Module
module "eks" {
  source = "./eks"

  # Pass the required variables to the EKS module
  cluster_name   = var.cluster_name
  vpc_id         = module.vpc.vpc_id
  private_subnets = module.vpc.private_subnets
}


