terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "s3" {
    bucket = "devops-directive-tfstate"
    key = "tf-infra/terraform.tfstate"
    region = "eu-central-1"
    dynamodb_table = "terraform-state-locking"
    encrypt = true
  } 
}