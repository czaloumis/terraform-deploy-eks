variable "region" {
  description = "AWS region to create resources in"
  type        = string
  default     = "eu-central-1"
}

variable "env_name" {
  description = "Enviroment Name"
  type        = string
  default     = "dev"
}

variable "vpc_name" {
  description = "Name of the VPC"
  default     = "dev-vpc"
}

variable "cluster_name" {
  description = "Name of the EKS cluster"
  default     = "dev-eks-cluster"
}

